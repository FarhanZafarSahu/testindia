<?php

namespace App\Http\Controllers\Api;

use App\Employee;
use App\Http\Controllers\Controller;
use App\Reward;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

//Constants 
use App\Constants\Message;

class ApiController extends Controller
{
       public function fetchNews(Request $request)
    {
        try
        {
			$dateFrom = Carbon::now();	
			$dataTo = Carbon::now()->subdays($request->NumberOfLastDays);
            $URL = 'https://gnews.io/api/v4/search?q='.$request->searchValue.'&token='.$request->token.'&country='.$request->countryCode.'&lang='.$request->language.'&max='.$request->maximumPost.'&from='.$dateFrom . '&to='. $dataTo ;
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => $URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 1000,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
          return [
            'data' => [
                'code' => 200,
                'message' => "G News Api Data.",
                'result' => json_decode($response),
                'errors' => null,
            ],
           'status' => true
        ];
        }
        catch (\Exception $e)
        {
            return $e->getMessage() ;
        }
    }
  public function fetchName(Request $request)
    {
        try
        {
			$endWithLetter = "n";
		
			$employees  =  Employee::where('First_name', 'LIKE' ,'%'.$endWithLetter)->get();
			$employeIds =  Reward::where('amount', '<',5100)->pluck('Employee_ref_id');
			$rewards    =  Employee::whereIn('Employee_id',$employeIds)->get();
			$joining 	=  Employee::whereDate('Joining_date', '<' ,'2019-02-25')->get();
			$result = [
			'LetterN' => $employees,
			'rewards' => $rewards,
			'joining' => $joining,
				];
          return [
            'data' => [
                'code' => 200,
                'message' => Message::REQUEST_SUCCESSFUL,
                'result' => $result,
                'errors' => null,
            ],
           'status' => true
        ];
        }
        catch (\Exception $e)
        {
             return [
            'data' => [
                'code' => 500,
                'message' => "Exception",
                'result' => null,
                'errors' => $e->getMessage(),
            ],
           'status' => false
        ];

        }
    }
}
